#ifndef FIRSTMSG_H
#define FIRSTMSG_H
#include <iostream>

class firstmsg
{
    public:
        firstmsg();
        virtual ~firstmsg();
        std::string getMsg();
    protected:
    private:
};

#endif // FIRSTMSG_H
