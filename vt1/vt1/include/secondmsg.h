#ifndef SECONDMSG_H
#define SECONDMSG_H
#include <iostream>

class secondmsg
{
    public:
        secondmsg();
        virtual ~secondmsg();
        std::string getMsg();
    protected:
    private:
};

#endif // SECONDMSG_H
