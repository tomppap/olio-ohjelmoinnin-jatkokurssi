#include <iostream>
#include <random>
#include <firstmsg.h>
#include <secondmsg.h>

using namespace std;

int main()
{
    int lkm = 0;
    bool passed = false;

    do {
        cout << "Anna tervehdyksen toistomäärä: ";
        cin >> lkm;

        if(!cin)
        {
            // user didn't input a number
            cin.clear(); // reset failbit
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //skip bad input
        }
        else
            passed = true;
    } while(!passed);


    for(int i=0; i<lkm; i++) {
        random_device rd;
        default_random_engine el(rd());
        uniform_int_distribution<int> dist(1,2);
        int random = dist(el);
        string msg = "";

        if(random == 1) {
            firstmsg* msg1 = new firstmsg();
            msg = msg1->getMsg();
            delete msg1;
        }
        else if(random == 2) {
            secondmsg* msg2 = new secondmsg();
            msg = msg2->getMsg();
            delete msg2;
        }

        cout << msg << endl;
    }

    return 0;
}
